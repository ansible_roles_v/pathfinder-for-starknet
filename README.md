Pathfinder for StarkNet
=========
Setup Pathfinder software for StarkNet.

Requirements
------------
Docker => 1.12  

Include role
------------
```yaml
- name: pathfinder  
  src: https://gitlab.com/ansible_roles_v/pathfinder/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: starknet-nodes
  gather_facts: true
  become: true
  roles:
    - pathfinder
```